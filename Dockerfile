FROM python:3.12-alpine AS base

# Install devcontainers/cli requirements
RUN apk add --update --no-cache g++ git jq make nodejs npm
RUN npm install -g @devcontainers/cli

# Add flexible json parser to handle jsonc (json with comments) / rjson (relaxed json) / json5
RUN wget -O jsonc-cli.tar.gz https://github.com/muhammadmuzzammil1998/jsonc-cli/releases/download/v1.0/jsonc-cli-linux-amd64.tar.gz \
    && gunzip jsonc-cli.tar.gz \
    && tar xf jsonc-cli.tar \
    && mv jsonc-cli-linux-amd64/jsonc-cli /usr/local/bin \
    && rm -rf jsonc-cli-linux-amd64 jsonc-cli.tar

###
FROM base AS ci

RUN apk add --update --no-cache docker docker-cli docker-cli-compose
COPY --from=hashicorp/vault:1.17 /bin/vault /bin/

# Default workspace
RUN mkdir /workspace
WORKDIR /workspace

###
FROM base AS devcontainer

# Default workspace
RUN mkdir /workspaces
WORKDIR /workspaces
