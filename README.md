# Geekstuff.dev / Devcontainers / Common / Docker images

The different projects in "Geekstuff.dev / Devcontainers" uses both devcontainers
for local development, and gitlab-ci images for different build or publishing needs.

This project builds and publishes those images using a single multi-stage Dockerfile.
