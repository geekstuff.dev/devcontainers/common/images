#!make

MAKEFLAGS = --no-print-directory

CI_REGISTRY_IMAGE ?= devcontainers/common/images
TAG ?= dev

all: builds

.PHONY: builds
builds:
	@$(MAKE) build-ci
	@$(MAKE) build-devcontainer

.PHONY: build-ci
build-ci: TARGET = ci
build-ci: .build

.PHONY: build-devcontainer
build-devcontainer: TARGET = devcontainer
build-devcontainer: .build

.PHONY: .build
.build:
	docker build \
		-t ${CI_REGISTRY_IMAGE}/${TARGET}:${TAG} \
		--target ${TARGET} \
		.
	@echo "Built image:"
	@echo "  ${CI_REGISTRY_IMAGE}/${TARGET}:${TAG}"

.PHONY: shell
shell:
	docker run --rm -it --entrypoint sh ${CI_REGISTRY_IMAGE}/ci:${TAG}

.PHONY: ci-build-push
ci-build-push: .build
ci-build-push:
	docker push ${CI_REGISTRY_IMAGE}/${TARGET}:${TAG}
